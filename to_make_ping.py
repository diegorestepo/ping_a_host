'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''


import platform
import subprocess


def ping(host, number_of_packages):
    option = '-n' if platform.system().lower() == 'windows' else '-c'

    return subprocess.run(['ping', option, number_of_packages, host])


def run():
    hosts = input('\nhosts = ').split(', ')

    number_of_packages = input('\nnumber of packages = ')

    print(f'\nPing to:  {hosts}\n')

    ping_host = [ping(host, number_of_packages) for host in hosts]
    print(*ping_host, sep='\n')


if __name__ == '__main__':
    run()
